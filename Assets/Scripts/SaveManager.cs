using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor.Build.Player;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class SaveManager : MonoBehaviour
{
    public List<int> Load()
    {
        var saveObject=JsonUtility.FromJson<Save>(PlayerPrefs.GetString("Save"));
        if(saveObject!=null)
        { 
           
            return  saveObject.scores.OrderByDescending(i=>i).ToList();
        }
        else
        {
            return new List<int>();
        }

    }
    public void Save(int score)
    {
        var SaveObject = new Save();
        SaveObject.scores = Load();
        SaveObject.scores.Add(score);
        PlayerPrefs.SetString("Save",JsonUtility.ToJson(SaveObject));
    }
}

public class Save
{
    public List<int> scores;
}