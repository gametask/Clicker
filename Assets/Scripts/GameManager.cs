using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    
    public List<GameObject> enemyPrefabs;
    public GameObject platformPrefab;
    [SerializeField] private float _size,_speed,_health,_difficulty;

    private Coroutine _spawnCoroutine;
    RaycastHit _hit;

    private Ray _ray;
    [SerializeField] private int _aliveMobs;

    public GameObject LoseScreen;
    public Button RetryButton;
    public Button BackButton;
    
    private int score;
    [SerializeField] public Text scoreText;

    [SerializeField] public Text GameScore;
    public SaveManager savemanager;
    private bool _finished;

    public Button FreezeButton;
    public Button KillButton;
    public GameObject UIContainer;
    public Button SlowButton;
    void Start()
    {
        Application.targetFrameRate = 60;
         StartCoroutine(SpawnBoost());
         _spawnCoroutine = StartCoroutine(SpawnEnemy());
         RetryButton.onClick.AddListener(() => SceneManager.LoadScene(1));
         BackButton.onClick.AddListener(() => SceneManager.LoadScene(0));
         KillButton.onClick.AddListener(KillAll);
         FreezeButton.onClick.AddListener(FreezeBoost);
         SlowButton.onClick.AddListener(SlowDown);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(_ray,out _hit,100.0f))
            {
                if (_hit.transform != null)
                {
                    if (_hit.transform.CompareTag("Enemy"))
                    {
                        _hit.transform.parent.GetComponent<Enemy>().Damage();
                    }
                }
            } 
        }

        if (_aliveMobs >= 10 && !_finished)
        {
            Lose();
        }
    }

    IEnumerator SpawnEnemy()
    {
        while (true)
        {
            var enemyPrefab = enemyPrefabs[Random.Range(0, enemyPrefabs.Count)];
            var enemy = Instantiate(enemyPrefab, new Vector3(Random.Range(-_size, _size), 0, Random.Range(-_size, _size)),
                Quaternion.identity,platformPrefab.transform);
            enemy.transform.GetComponent<Enemy>().SetParams(_difficulty,this);
            _aliveMobs++;
            yield return new WaitForSeconds(Mathf.Clamp(Random.Range(2f,4f) - _difficulty, 0.7f,4f));
        }
    }

    public void IncreaseDiff()
    {
        AddPoint();
        _difficulty += 0.05f;
        _aliveMobs--; 
    }

    public void Lose()
    {
        _finished = true;
        foreach (var enemy in   GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy.transform.parent.gameObject);
        }
        savemanager.Save(score);
        StopCoroutine(_spawnCoroutine);
        platformPrefab.SetActive(false);
        LoseScreen.SetActive(true);
        UIContainer.SetActive(false);
    }

    public void FreezeBoost()
    {
        FreezeButton.gameObject.SetActive(false);
        StartCoroutine(FreezeSpawn());
    }

    IEnumerator FreezeSpawn()
    {
        StopCoroutine(_spawnCoroutine);
        yield return new WaitForSeconds(3f);
        _spawnCoroutine = StartCoroutine(SpawnEnemy());
    }

    public void KillAll()
    {
        KillButton.gameObject.SetActive(false);
        _aliveMobs = 0;
        foreach (var enemy in   GameObject.FindGameObjectsWithTag("Enemy"))
        {
            Destroy(enemy.transform.parent.gameObject);
            AddPoint();
        }
    }

    public void SlowDown()
    {
        SlowButton.gameObject.SetActive(false);
        foreach (var enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
          enemy.transform.parent.GetComponent<Enemy>().SlowDown();
        }
    }

    public void AddPoint()
    {
        scoreText.text = $"Score: {++score}";
        GameScore.text = $"Score: {score}";
    }

    IEnumerator SpawnBoost()
    {
       while(true)
       {
           yield return new WaitForSeconds(Random.Range(3f,5f));
           var SpawnBoost = (int) Math.Round(Random.Range(0f, 2f));
           switch (SpawnBoost)
           {
               case 0:
                   KillButton.gameObject.SetActive(true);
                   break;
               case 1:
                   FreezeButton.gameObject.SetActive(true);
                   break;
               case 2:
                   SlowButton.gameObject.SetActive(true);
                   break;
           }
       }
    }
}
