using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Button PlayGameButton;

    public Button ExitGameButton;

    public Button LeaderBoardButton;

    public Button CreditsButton;
    public Button BackFromLead;
    public Button BackFromCredit;
    public GameObject Menu;
    public GameObject LeaderBoard;
    public GameObject Credit;

    public Text LeaderBoardText;
    public SaveManager SaveManager;
    void Start()
    {
        foreach (var score in SaveManager.Load())
        {
            LeaderBoardText.text += $"{score}\n";
        }
        
        PlayGameButton.onClick.AddListener(StartGame);
        ExitGameButton.onClick.AddListener(() =>  Application.Quit());
        LeaderBoardButton.onClick.AddListener(ShowLeaderBoard);
        CreditsButton.onClick.AddListener(ShowCredits);
        BackFromLead.onClick.AddListener(ShowLeaderBoard);
        BackFromCredit.onClick.AddListener(ShowCredits);
    }

    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ShowCredits()
    {
        Menu.SetActive(!Menu.activeSelf);
        Credit.SetActive(!Credit.activeSelf);
    }

    public void ShowLeaderBoard()
    {
        Menu.SetActive(!Menu.activeSelf);
        LeaderBoard.SetActive(!LeaderBoard.activeSelf);
    }
}
