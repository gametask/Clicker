using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    private float _speed, _health,_maxHealth;
    [SerializeField]
    private GameManager _gameManager;
    private Vector3 _target;
    public Slider HealthBar;
    private AudioSource _audioSource;
    public GameObject container;
    void Start()
    {
        StartCoroutine(ChangeDir());
        _audioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        var step =  _speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _target, step);
    }

    IEnumerator ChangeDir()
    {
        while (true)
        {
            _target = new Vector3(Random.Range(-10f,10f),0,Random.Range(-10,10f));
            yield return new WaitForSeconds(1f);
        }
    }

    public void SetParams(float difficulty,GameManager gameManager)
    {
        
        _speed *= difficulty;
        _health *= difficulty;
        _maxHealth = _health;
        _gameManager = gameManager;
    }
    
    public void Damage()
    {
        _health -= 10;
        _audioSource.PlayOneShot(_audioSource.clip);
        HealthBar.value = _health / _maxHealth;
        if (_health <= 0)
        {
            _gameManager.IncreaseDiff();
            container.SetActive(false);
            Destroy(gameObject,1f);
        }
    }
    public void SlowDown()
    {
        _speed /= 2;
    }
}
